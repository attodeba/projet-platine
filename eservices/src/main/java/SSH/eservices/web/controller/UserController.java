package SSH.eservices.web.controller;

import SSH.eservices.model.Course;
import SSH.eservices.model.User;
import SSH.eservices.web.dto.ResponseTO;
import SSH.eservices.web.dto.UserTO;
import SSH.eservices.web.services.Itf.UserService;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class UserController {

    @Autowired
    UserService userService;
    ResponseTO response;

    /**
     *@param email User email
     *@return User found if exists
     */
    @GetMapping(path = "/users/{email}", produces = MediaType.APPLICATION_JSON_VALUE)
    public User getUser(@PathVariable("email") String email) throws Exception {
        return userService.getUser(email);
    }

    /**
     *@return List of all users
     */
    @GetMapping(path = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<User> getAllUsers(){
        return userService.getUsers();
    }


    /**
     * @param email
     * @param password
     * @return User corresponding to given credentials if exists
     */
    @PostMapping(path = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public User login(String email, String password) {
        return userService.getUserByEmailAndPassword(email, password);
    }

    /**
     * return user or null if the user already exist
     *
     * @param userTo
     * @return String
     */
    @ResponseBody
    @PostMapping(path = "/users", consumes=MediaType.APPLICATION_JSON_VALUE)
    public ResponseTO<User> addUser(@RequestBody UserTO userTo) {
        response = new ResponseTO();
        try {
          response.setData( userService.createUser(userTo.toUser()) );
          response.setStatus(String.valueOf(HttpStatus.OK));
          return response;
        }catch (Exception  e){
            response.setErrors(e.getMessage());
            response.setStatus("500");
        return response;
      }

    }

    /**
     * @param email
     * @param oldPassword
     * @param newPassword
     * @return
     */
    @PostMapping(path = "changePass", produces = MediaType.APPLICATION_JSON_VALUE)
    public String changePassword(String email, String oldPassword, String newPassword) {
        User user = userService.getUserByEmailAndPassword(email, oldPassword);
        if (null != user) {
            try {
                user.setPassword(sha1(newPassword));
            } catch (Exception e) {
                return "Error changing the password: " + e.toString();
            }
        }
        return "Password succesfully changed! (id = " + user.getEmail() + ")";
    }

    /**
     *@param userToUpdate
     * @return Updated user
     */
    @PutMapping(path = "/users/{email}", produces = MediaType.APPLICATION_JSON_VALUE)
    public User editUserDAO(@RequestBody User userToUpdate) throws Exception {
        return userService.editUser(userToUpdate);
    }


    /**
     *@param userEmail
     * @return True if user is deleted else false
     */
    @DeleteMapping(path = "/users/{email}", produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean deleteUser(@PathVariable("email") String userEmail){
        return userService.deleteUser(userEmail);
    }

    /**
     *
     * @param userEmail
     * @return List of all courses belong to user #userEmail
     */
    @GetMapping(path = "/courses/{email}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Course> getCourses(@PathVariable("email") String userEmail) {
        return userService.listUserCourses(userEmail);
    }

    /**
     * Sha1 : crypte de mot de passe
     *
     * @param password
     * @return
     */
    public static String sha1(String password) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "*****";
        }
        byte[] sha1sum = md.digest(password.getBytes());
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < sha1sum.length; i++) sb.append(String.format("%02x", sha1sum[i]));
        return sb.toString();
    }

}
