package SSH.eservices.web.controller;

import SSH.eservices.model.Path;
import SSH.eservices.repository.PathRepository;
import SSH.eservices.web.dto.ResponseTO;
import SSH.eservices.web.services.Itf.IPathService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/path")
public class PathRestController {

    @Autowired
    PathRepository pathRepository;

    @Autowired
    IPathService iPathService;
    /**
     * get a path by id
     * @param id
     * @return path bean
     */
    @GetMapping(path = "/{id}.html", produces = "application/json")
    public ResponseTO<Path> get(int id) throws Exception {
        ResponseTO<Path> response = new ResponseTO<>() ;
        Path foundedPath = iPathService.getPath(id);
        response.setData(foundedPath);
        response.setStatus("OK");
        /*try {
          if(id>0){
              Path path = pathRepository.findOne((long)id);
              response.setData(path);
          }
          else{
            response.setData(null);
            response.setErrors("Technical error! Can not find path with id [ "+Integer.toString(id)+" ] ");
          }
        }catch (Exception e){
            return null;
        }*/
        return response;
    }

    /**
     *
     * @param path
     * @return
     */
    /*@ResponseBody
    @PutMapping(path = "/add", consumes = "application/json")
    public ResponseTO<Boolean> post(@RequestBody final Path path ){
        ResponseTO<Boolean> response = new ResponseTO<>();
        // todo: verify path is valid
        try {
              pathRepository.save(path);
              response.setData(true);
        }
        catch (Exception e){
            response.setData(false);
            response.setErrors("technical error");
        }
        return  response;
    }

    *//**
     *
     * @param id
     * @return
     *//*
    @GetMapping(path = "/{id}/survey.html", produces = "application/json")
    public ResponseTO<List<Survey>> getSurvey(int id) {
        ResponseTO<List<Survey>> response = new ResponseTO<>() ;
        try {
            if(id>0){
                Path path = pathRepository.findOne((long)id);
                if(null!=path){
                    response.setData(path.getSurveys());
                }

            }
            else{
                response.setData(null);
                response.setErrors("Technical error! Can not find survey  with id [ "+Integer.toString(id)+" ] ");
            }
        }catch (Exception e){
            return null;
        }
        return response;
    }*/


}
