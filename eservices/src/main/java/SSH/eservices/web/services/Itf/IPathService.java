package SSH.eservices.web.services.Itf;

import SSH.eservices.model.Path;

public interface IPathService {

	Path createPath(Path newPath);
	Path editPath(Path pathToEdit) throws Exception;
	boolean deletePath(Path pathToDelete);
	Path getPath(int id) throws Exception;

}
