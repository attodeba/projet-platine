package SSH.eservices.web.services.Itf;

import SSH.eservices.model.Course;
import SSH.eservices.model.User;
import SSH.eservices.web.dto.UserTO;

import java.util.List;

public interface UserService {

	User createUser(User newUser) throws Exception;

	User createUser(String email, String userName, String password) throws Exception;

	User editUser(User userToEdit) throws Exception;

	boolean deleteUser(String userEmail);

	List<User> getUsers();

	User getUser(String email) throws Exception;

	User getUserByEmailAndPassword(String email, String password);

	boolean exists(String email);

	List<Course> listAllCourses();

	public List<Course> listUserCourses(String email);

}
