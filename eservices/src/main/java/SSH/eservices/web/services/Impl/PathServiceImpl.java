package SSH.eservices.web.services.Impl;

import SSH.eservices.model.Path;
import SSH.eservices.repository.PathRepository;
import SSH.eservices.web.services.Itf.IPathService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("pathService")
public class PathServiceImpl implements IPathService {

	@Autowired
	PathRepository pathRepository;

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Path createPath(Path newPath) {
		Path pathToCreate = null;
		if(newPath != null){
			if(newPath.getFrom() != null && newPath.getTo() != null){
				try{
					pathToCreate = pathRepository.save(newPath);
				}catch (Exception iae){
					iae.printStackTrace();
				}
			}else{
				throw new IllegalArgumentException("Cannot create path without its from and to Points");
			}
		}else {
			throw new IllegalArgumentException("Cannot create null path");
		}
		return pathToCreate;
	}

	@Override
	public Path editPath(Path pathToEdit) throws Exception {
		Path foundedPath = getPath(pathToEdit.getId());
		foundedPath.setFrom(pathToEdit.getFrom());
		foundedPath.setTo(pathToEdit.getTo());
		foundedPath.setSurveys(pathToEdit.getSurveys());
		return entityManager.merge(foundedPath);
	}

	@Override
	public boolean deletePath(Path pathToDelete) {
		try{
			pathRepository.delete(pathToDelete);
			return true;
		}catch (HibernateException he) {
			throw new IllegalArgumentException("Unknown path to delete !");
		}
	}

	@Override
	public Path getPath(int id) throws Exception {
		Path foundedPath = pathRepository.findOne(Long.parseLong(String.valueOf(id)));
		if(foundedPath == null){
			throw new Exception("Path with id ["+id+"] doesn't exists !");
		}
		return foundedPath;
	}
}
