package SSH.eservices.web.dto;

import org.springframework.context.annotation.Bean;


public class ResponseTO<T>{
   public String status;
   public String errors;
   public Object data;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrors() {
        return errors;
    }

    public void setErrors(String errors) {
        this.errors = errors;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data= new Object();
        this.data = data;
    }
}
