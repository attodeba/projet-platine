package SSH.eservices.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * App config!
 *
 */
@SpringBootApplication
@EntityScan(basePackages = "SSH.eservices.model")
@EnableJpaRepositories(basePackages = {"SSH.eservices.repository"})
@ComponentScan(basePackages = {"SSH.eservices.web"})
public class App{

    private static final Logger log = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        SpringApplication.run(App.class);
    }
}
