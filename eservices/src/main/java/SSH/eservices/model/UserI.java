package SSH.eservices.model;

import java.util.List;

public interface UserI {

	public String getUsername();
	public String getEmail();
	public String getPassword();
	public List<Course> getCourses();

}
