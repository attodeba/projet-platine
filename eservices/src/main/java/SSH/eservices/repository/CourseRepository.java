package SSH.eservices.repository;

import SSH.eservices.model.Course;
import org.springframework.data.repository.CrudRepository;

public interface CourseRepository
		extends CrudRepository<Course, Long> {
}
